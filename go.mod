module bitbucket.org/perfectgentlemande/go-zip-http-example

go 1.17

require (
	github.com/go-chi/chi v1.5.4
	golang.org/x/sync v0.0.0-20210220032951-036812b2e83c
)
