package main

import (
	"archive/zip"
	"context"
	"errors"
	"fmt"
	"io"
	"log"
	"net/http"
	"os"
	"os/signal"
	"path"
	"syscall"

	"github.com/go-chi/chi"
	"github.com/go-chi/chi/middleware"

	"golang.org/x/sync/errgroup"
)

const (
	filesFolder = "files"
)

func zipHandler(w http.ResponseWriter, r *http.Request) {
	zipWriter := zip.NewWriter(w)
	defer zipWriter.Close()

	dirEntries, err := os.ReadDir(filesFolder)
	if err != nil {
		log.Println("cannot read dir:", err)
		return
	}

	files := make([]*os.File, 0, len(dirEntries))
	for i := range dirEntries {
		if dirEntries[i].IsDir() {
			continue
		}

		file, errOpen := os.Open(path.Join(filesFolder, dirEntries[i].Name()))
		if errOpen != nil {
			log.Println("cannot open file:", errOpen)
			return
		}
		files = append(files, file)
	}
	defer func() {
		for i := range files {
			if err = files[i].Close(); err != nil {
				log.Println("cannot close file:", err)
			}
		}
	}()

	for i := range files {
		zw, errWr := zipWriter.Create(files[i].Name())
		if errWr != nil {
			log.Println("cannot create zip writer:", errWr)
			return
		}
		if _, err = io.Copy(zw, files[i]); err != nil {
			log.Println("cannot copy file:", err)
			return
		}
	}

	w.Header().Set("Content-Type", "application/zip")
}

func main() {
	ctx := context.Background()
	ctx, cancel := signal.NotifyContext(ctx,
		syscall.SIGHUP,
		syscall.SIGINT,
		syscall.SIGTERM,
		syscall.SIGQUIT)
	defer cancel()

	r := chi.NewRouter()
	r.Use(middleware.Logger)
	r.Get("/", zipHandler)

	const addr = ":3000"
	srv := http.Server{
		Handler: r,
		Addr:    addr,
	}

	log.Println("starting server on addr:", addr)
	rungroup, ctx := errgroup.WithContext(ctx)
	rungroup.Go(func() error {
		if errSrv := srv.ListenAndServe(); errSrv != nil && !errors.Is(errSrv, http.ErrServerClosed) {
			return fmt.Errorf("listen and serve error: %w", errSrv)
		}

		return nil
	})
	rungroup.Go(func() error {
		<-ctx.Done()

		if errShutdown := srv.Shutdown(context.Background()); errShutdown != nil {
			return fmt.Errorf("shutdown http server %w", errShutdown)
		}

		return nil
	})
	err := rungroup.Wait()
	if err != nil {
		log.Fatal("run group exited because of error:", err)
	}

	log.Println("server exited properly")
}
